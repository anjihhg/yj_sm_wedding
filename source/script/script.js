const Utils = {
	modulate: function(value, rangeA, rangeB, limit) {
		var fromHigh, fromLow, result, toHigh, toLow
		if (limit == null) {
			limit = false
		}
		fromLow = rangeA[0], fromHigh = rangeA[1]
		toLow = rangeB[0], toHigh = rangeB[1]
		result = toLow + (((value - fromLow) / (fromHigh - fromLow)) * (toHigh - toLow))
		if (limit == true) {
			if (toLow < toHigh) {
				if (result < toLow) {
					return toLow
				}
				if (result > toHigh) {
					return toHigh
				}
			} else {
				if (result > toLow) {
					return toLow
				}
				if (result < toHigh) {
					return toHigh
				}
			}
		}
		return result
  }
}

class Init {
  constructor() {
    window.addEventListener('scroll', this.onScroll);
    window.addEventListener('resize', this.resizeHandler);

    this.resizeHandler()
    this.setSwiper()
    this.clickButton()

    window.sr = new ScrollReveal({
      viewFactor: 0.2,
      scale: 1,
      distance: '30px',
      duration: 750,
    })
    sr.reveal('.sr')
    sr.reveal('.v-line', { scale: 0, duration: 1000, reset: true})
    sr.reveal('.word', { scale: 0, delay: 0.25, duration: 1000, reset: true}, 150)
  }

  setSwiper() {
    var mySwiper = new Swiper ('.swiper-container', {
        pagination: '.swiper-pagination',
        spaceBetween: 1,
      })
      mySwiper.slideTo(5, 0, false)
      setTimeout(function () {
        mySwiper.slideTo(0, 2000, false)
      }, 200);
  }

  runScroll() {
    this.scrollTo(document.body, 100, 600);
  }

  scrollTo(element, to, duration) {
    if (duration <= 0) return
    var difference = to - element.scrollTop
    var perTick = difference / duration * 10

    setTimeout(function() {
      element.scrollTop = element.scrollTop + perTick
      if (element.scrollTop === to) return
      scrollTo(element, to, duration - 10)
    }, 10)
  }

  onScroll(e) {
    const wrapper = document.getElementById('swrapper')
    const welcome = document.getElementById('welcome-text')
    const _container = document.getElementById('swwrapper')
    const _y = window.scrollY
    const _ty = Utils.modulate(_y, [0, 500], [0, 200])
    const _v = Utils.modulate(_y, [0, 250], [1, 0])
    wrapper.style.transform = `translate3d(0, ${-_ty}px, 0)`
    welcome.style.opacity = _v
  }

  resizeHandler() {
    const wel = document.getElementById('welcome-text')
    const wrapper = document.getElementById('swrapper')

    wel.style.marginTop = window.innerHeight / 1.5 + "px"
    wrapper.style.height = window.innerHeight + "px"
  }

  clickButton() {
    const button = document.getElementById('info-button')
    button.addEventListener('click', () => {
      console.log("test")
      $('body').animate({scrollTop: $(window).height()}, 600)
    })

  }
}


new Init
